var paper;

paper.install(window);

var theMouseY;

$( document ).on( "mousemove", function( event ) {
  theMouseY = event.pageY;
});

window.onload = function() {
		paper.setup('myCanvas');
		// Create a simple drawing tool:

        drawFlower(1);
        randomPlace(50);
        drawPath();

        goPoint();

        view.onFrame = function(event) {

            if(pointPlace !== 10){
                pointPlace++;
            }
            else{
                pointPlace = 0;
                myPoint.position.y = (Math.random()*100)+300;
                myPoint.position.x = -(Math.random()*800);
            }


            for (var i = 0; i < myDec.length; ++i) {
                //Each frame, rotate the path by 3 degrees:

                var sinus = Math.cos(event.time/i * 10);
                var sinus2 = Math.sin(event.time*i);
                // Change the y position of the segment point:
                //segment.point.y = sinus * height + 100;
               // myDec[i].rotate(i/5);
                if(myPoint.position.y != myDec[i].position.y){
                    myDec[i].position.y = myDec[i].position.y + ((sinus));
                        //+(-(theMouseY))+300;
                }

                var getRad = -(sinus2*50) * (Math.PI/180);

                myDec[i].rotation = getRad;

                if(myDec[i].position.x <= 2000){
                    myDec[i].position.x += 3;
                }
                else{
                    myDec[i].position.x = myPoint.position.x;
                    myDec[i].position.y = myPoint.position.y;
                }

                myPaths[5].segments[0].point.y = myDec[i].position.y;
                myPaths[5].segments[0].point.x = myDec[i].position.x;

                //myDec[i].skew(sinus);
            }

            for (var i = 0; i < myPaths.length; ++i) {
                myPaths[i].segments[0].point.y = myDec[i].position.y;
                myPaths[i].segments[1].point.y = myDec[i+1].position.y;

                myPaths[i].segments[0].point.x = myDec[i].position.x;
                myPaths[i].segments[1].point.x = myDec[i+1].position.x;
            }

            for (var i = 0; i < myPedals.length; ++i) {
                myPedals[i].rotation -= theMouseY/100;
            }

             for (var i = 0; i < storeDust.length; ++i) {
                if(storeDust[i].position.x <= 1200){
                    storeDust[i].position.x += i/10;
                }
                 else{
                    storeDust[i].position.x = 0;
                     storeDust[i].position.y = Math.random()*600;
                 }
                if(storeDust[i].position.y <= 600){
                    if(myPoint.position.y != storeDust[i].position.y){
                        storeDust[i].position.y += 10;
                    }
                }
                 else{
                    storeDust[i].position.y = 0;
                 }

            }

        }
	}



function randomPlace(amount){
    for (var i = 0; i < amount; ++i) {
        var xPlace = -(i)*50;
        var yPlace = (Math.random()*100)+300;
        drawShape(5, xPlace, yPlace);
    }
}

var myColors = new Array('#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff');
var myDec = new Array();

function drawShape(sides, theX, theY){
    var getColor = Math.round((Math.random()*myColors.length));
    var decahedronSize = Math.random()*20;
        //(event.pageY - event.pageX)/5;
    var decahedron = new Path.RegularPolygon(new Point(theX, theY), sides, decahedronSize);

    decahedron.fillColor = "#8FBC1E"
        //myColors[getColor];
    //decahedron.selected = true;
    decahedron.rotation = 90;
    //decahedron.blendMode = 'multiply';
    decahedron.curvature = 0;
    decahedron.selectedColor = "#ffffff";
    //decahedron.skew(20);
    decahedron.segments[0].selected = false;

    myDec.push(decahedron);

}

var myPaths = new Array();

function drawPath(){
    for (var i = 0; i < myDec.length-1; ++i) {
        var getColor2 = Math.round((Math.random()*myColors.length));
        var center = myDec[i].position;
        var center2 = myDec[i+1].position;
        var myPath = new Path();

        myPaths.push(myPath);

        myPath.strokeColor = "#8FBC1E";
        myPath.strokeWidth = 1;
        myPath.add(new Point(center, center));
        myPath.add(new Point(center2, center2));

        goDust();
        goDust();
        goDust();

        drawFlower(i/10);

    }

}

function goSVG(){

}

var storeDust = new Array();
function goDust(){
    var circleSize = Math.random()*2;
    var circleX = Math.random()*800;
    var circleY = Math.random()*600;
    var myCircle = new Path.Circle(new Point(circleX, circleY), circleSize);
    myCircle.fillColor = 'black';

    storeDust.push(myCircle);
}

var myPoint;
var pointPlace = 0;
function goPoint(){
    var circleX = Math.random()*800;
    var circleY = Math.random()*600;
    myPoint = new Path.Circle(new Point(circleX, circleY), 10);
    //myPoint.fillColor = 'none';
}


var myPaths2 = new Array();
var myPedals = new Array();
var pedalColors = new Array("#ff0000", "#ffffff", "#ffcc00", "#ff0000");

function drawFlower(flowerAmount){
            var myPath = new Path();

            var randomX = Math.round(Math.random()*1200);
            var randomX2 = randomX + (Math.random()*100)-50;
            var randomHeight = Math.round(Math.random()*300)+200;

            var randomSides = 3

            myPaths2.push(myPath);

            myPath.strokeColor = "#ff0000";
            myPath.strokeWidth = 1;
            myPath.add(new Point(randomX, 800));
            myPath.add(new Point(randomX2, 600));
            myPath.add(new Point(randomX, randomHeight));

            var petalAmounts = flowerAmount;

            for (var i = 0; i < petalAmounts; ++i) {
                    var getColor = Math.round((Math.random()*pedalColors.length));
                    var decahedronSize = i+40;
                    var decahedron = new Path.RegularPolygon(new Point(myPath.segments[2].point.x, myPath.segments[2].point.y), randomSides, decahedronSize);


                    myPedals.push(decahedron);
                    decahedron.pivot = new Point(myPath.segments[2].point.x,myPath.segments[2].point.y);
                    decahedron.rotation = (360/petalAmounts)*i;
                    decahedron.fillColor = pedalColors[getColor];
                    decahedron.blendMode = 'hard-light';
                    decahedron.curvature = 50;
                    decahedron.selectedColor = "#ffffff";
                   // decahedron.segments[0].selected = false;
            }

}


